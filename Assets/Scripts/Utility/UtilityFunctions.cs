﻿using UnityEngine;

public static class UtilityFunctions
{
	public static float Map(float value, float initialMin, float initialMax, float destinationMin, float destinationMax)
	{
		var t = (value - initialMin) / (initialMax - initialMin);
		return Mathf.Lerp(destinationMin, destinationMax, t);
	}

	public static Vector2 RotateVector2(float angle, Vector2 v)
    {
        var x = v.x * Mathf.Cos(angle * Mathf.Deg2Rad) - v.y * Mathf.Sin(angle * Mathf.Deg2Rad);
        var y = v.x * Mathf.Sin(angle * Mathf.Deg2Rad) + v.y * Mathf.Cos(angle * Mathf.Deg2Rad);

        return new Vector2(x, y);
    }

    public static void DebugDrawCircle(Vector3 position, float radius, Color color)
    {
        var inc = 36f;
        var angle = 360f / inc;
        var a = position + Vector3.up * radius;

        for (int i = 0; i < inc; i++)
        {
            var b = a - position;
            b = UtilityFunctions.RotateVector2(angle, b);
            b += position;
            Debug.DrawLine(a, b, color);
            a = b;
        }
    }
}
