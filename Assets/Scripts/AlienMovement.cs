﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class AlienMovement : MonoBehaviour
{
    public float MaxSpeed = 12f, 
        Acceleration = 4f,
        Friction = 4f;

    [HideInInspector]
    public float Speed = 0f;

    [HideInInspector]
    public Vector3 SeekPosition;

    private Vector3 _currentDirection; 
    private Rigidbody2D _rigidbody;
    private AlienHorde _horde;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _horde = GetComponentInParent<AlienHorde>();
        SeekPosition = transform.position;
    }
	
    void Update()
    { 

        var seekDir = Seek(SeekPosition);
        var separateDir = Separate();
        var avoidTerrainDir = AvoidTerrain();

        _currentDirection = seekDir + separateDir + avoidTerrainDir;

        if (GameManager.Instance.DEBUG)
        {
            Debug.DrawLine(transform.position, transform.position + avoidTerrainDir * 100f, Color.magenta);
            Debug.DrawLine(transform.position, transform.position + seekDir * 100f, Color.red);
            Debug.DrawLine(transform.position, transform.position + separateDir * 100f, Color.blue);
            Debug.DrawLine(transform.position, transform.position + _currentDirection * 100f, Color.yellow);
        }

        Speed = _currentDirection.magnitude > 0 ? Mathf.Clamp(Speed + Acceleration * Time.deltaTime, 0f, MaxSpeed) 
                : Speed = Mathf.Clamp(Speed - Friction, 0f, MaxSpeed);

        if (Vector2.Distance(SeekPosition, transform.position) < 10)
        {
            Speed = 0f;
        }

        _rigidbody.velocity = _currentDirection.normalized * Speed;
        
    }

    public Vector3 Separate()
    {
        var neighbours = Physics2D.OverlapCircleAll(transform.position, _horde.SeparationRadius, 1 << LayerMask.NameToLayer("Alien"));
        UtilityFunctions.DebugDrawCircle(transform.position, _horde.SeparationRadius, Color.green);

        var sum = Vector3.zero;
        foreach (var n in neighbours)
        {
            if (n.gameObject == gameObject)
                continue;

            var diff = transform.position - n.transform.position;
            sum += diff.normalized;
        }

        if (neighbours.Length - 1 > 0)
            sum /= (neighbours.Length - 1);

        return sum.normalized;
    }

    public Vector3 AvoidTerrain()
    {
        var neighbours = Physics2D.OverlapCircleAll(transform.position, _horde.SeparationRadius , 1 << LayerMask.NameToLayer("Terrain"));
        UtilityFunctions.DebugDrawCircle(transform.position, _horde.SeparationRadius, Color.red);

        var sum = Vector3.zero;
        foreach (var n in neighbours)
        {
            if (n.gameObject == gameObject)
                continue;

            var diff = transform.position - n.transform.position;
            sum += diff.normalized;
        }

        if (neighbours.Length - 1 > 0)
            sum /= (neighbours.Length - 1);

        return sum.normalized;
    }

    public Vector3 Seek(Vector3 s)
    {
        return (SeekPosition - transform.position).normalized;
    }
}
