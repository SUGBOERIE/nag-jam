﻿using UnityEngine;
using System.Collections.Generic;

public class LevelGenerator : MonoBehaviour {

	public LevelType Level;
	private int[] _rotationOptions = new int[4];
	private float[] _rotationChances = new float[4];
	private int _totalTiles; 
	private float _defaultFloorMakerChance;
	private float _1by1Chance;
	private float _2by2Chance;
	private float _3by3Chance;
	private float _5by5Chance;

	private bool _hasGeneratedWalls;
	public GameObject WallObject;
	public float TileSize;
	public int tileBufferNo;

	public LayerMask Mask1;
	public LayerMask Mask2;

	public List<GameObject> Tiles = new List<GameObject>();
	public GameObject FloorMaker;

	// Use this for initialization
	void Start () {
		SetupLevelTypeProperties ();
		GameObject floorMaker =(GameObject) Instantiate(FloorMaker, transform.position, transform.rotation);
		floorMaker.GetComponent<FloorMaker> ().SetupFloorMaker (_rotationOptions, _totalTiles, _defaultFloorMakerChance, _2by2Chance, 
		                                                        _3by3Chance, this, _rotationChances, _5by5Chance, _1by1Chance);
		GameObject.FindGameObjectWithTag ("Player").transform.position = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Q))
		{
			Application.LoadLevel(Application.loadedLevel);
		}

		if(Tiles.Count >= _totalTiles && !_hasGeneratedWalls)
		{
			GenerateWalls();
			_hasGeneratedWalls = true;
		}
	}

	private void GenerateWalls()
	{
		float minX =0;
		float maxX =0;
		float minY =0;
		float maxY =0;
		for(var i = 0; i <Tiles.Count; i++)
		{
			Vector3 curTilePos = Tiles[i].transform.position;
			if(curTilePos.x < minX) minX = curTilePos.x;
			if(curTilePos.x > maxX) maxX = curTilePos.x;
			if(curTilePos.y < minY) minY = curTilePos.y;
			if(curTilePos.y > maxY) maxY = curTilePos.y;
		}

		minX -= tileBufferNo * TileSize;
		maxX += tileBufferNo * TileSize;
		minY -= tileBufferNo * TileSize;
		maxY += tileBufferNo * TileSize;

		Vector3 startPos = new Vector3(minX, minY, 0f);
		Vector3 endPos = new Vector3(maxX, maxY, 0f);
		float xDistance = maxX - minX;
		float yDistance = maxY - minY;
		int xNo = Mathf.RoundToInt(xDistance / TileSize);
		int yNo = Mathf.RoundToInt(yDistance / TileSize);

		for(var i = 0; i <xNo; i++)
		{
			for(var j = 0; j <xNo; j++)
			{

				Vector3 spawnPos =startPos+new Vector3(i*TileSize,j*TileSize,0f);
				if(!IsolatedTile(spawnPos) && !ExistingTile(spawnPos)){
					GameObject wall = (GameObject)Instantiate(WallObject, spawnPos, transform.rotation);
					wall.transform.parent = GameObject.Find("Walls").transform;
				}
			}
		}
	}

	private bool ExistingTile(Vector3 originPos)
	{
		Vector3 origin = originPos + new Vector3 (0f, 0f, -1f);
		return (Physics2D.Raycast (origin, transform.forward, 2f, Mask1).collider != null);
	}

	private bool IsolatedTile(Vector3 origin)
	{
		float spriteSize = 32f;
		bool leftRay = Physics2D.Raycast (origin, -transform.right, spriteSize + 1f, Mask1).collider != null;//Left
		bool rightRay = Physics2D.Raycast (origin, transform.right, spriteSize + 1f, Mask1).collider != null;//Right
		bool upRay = Physics2D.Raycast (origin, transform.up, spriteSize + 1f, Mask1).collider != null;//Up
		bool downRay = Physics2D.Raycast (origin, -transform.up, spriteSize + 1f, Mask1).collider != null;//Down

		bool downRightRay = Physics2D.Raycast (origin, (-transform.up+transform.right), spriteSize + 1f, Mask1).collider != null;//Down Right
		bool downLeftRay = Physics2D.Raycast (origin, (-transform.up+-transform.right), spriteSize + 1f, Mask1).collider != null;//Down left
		bool upRightRay = Physics2D.Raycast (origin, (transform.up+transform.right), spriteSize + 1f, Mask1).collider != null;//Up right
		bool upLeftRay = Physics2D.Raycast (origin, (transform.up+-transform.right), spriteSize + 1f, Mask1).collider != null;//Up left

		return !(leftRay || rightRay || upRay || downRay || downRightRay || downLeftRay || upRightRay || upLeftRay) ;
	}

	private bool TilePositionExists(Vector3 searchPos)
	{
		for(var i = 0; i < Tiles.Count; i++)
		{
			if(searchPos == Tiles[i].transform.position){
				return true;
			}
		}
		
		return false;
	}

	private void SetupLevelTypeProperties()
	{
		switch (Level) 
		{
			case LevelType.MoonBase:
			_rotationOptions [0] = 90;
			_rotationOptions [1] = 270;
			_rotationOptions [2] = 180;
			_rotationOptions [3] = 0;

			_rotationChances[0] = 0.05f;
			_rotationChances[1] = 0.05f;
			_rotationChances[2] = 0.025f;//180
			_rotationChances[3] = 0.8f;//Straight

			_totalTiles = Random.Range(2000, 3000);
			_defaultFloorMakerChance = 0.1f;
			_1by1Chance = 0.8f;
			_2by2Chance = 0.05f;
			_3by3Chance = 0.05f;
			_5by5Chance = 0.1f;
			break;
		}
	}
}
