﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class TestSFX : MonoBehaviour
{
	//place sounds into this array in the inspector
	public AudioClip[] BulletSounds;
	public AudioClip[] ExplosionSounds;
    public AudioClip[] Music;
    public AudioMixerGroup MusicBus;
    void Update()
    {
    	// When you press z it plays the first sound in the BulletSounds array at middle of the screen
    	// So, we'd just say for example when you shoot instead 
    	if (Input.GetKeyDown(KeyCode.Z))
    	{
    		// This function can be called anywhere in the code and place the sound in the 
    		// scene the last parameter is the volume.
    		Sound.PlaySoundAt(BulletSounds[0], Vector3.zero, 1f);
    	}

    	// When you press x it plays the first sound at middle of the screen
    	// So, we'd just say for example when you shout instead 
    	if (Input.GetKeyDown(KeyCode.X))
    	{
            // This function takes in an array of sounds (collection of sounds)
            // then randomly selects one of them and plays them with volume 1
            //Sound.PlaySoundEffectAt(ExplosionSounds, Vector3.zero, 1f);
            Sound.PlaySoundLoopEffectAt(Music, Vector3.zero, MusicBus);
    	}
    }
}
