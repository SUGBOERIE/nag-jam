﻿using UnityEngine;
using System.Collections.Generic;

public class EffectManager : MonoBehaviour {

	public GameObject BloodParticle;
	public GameObject ScreenFill; 
	public GameObject EffectText;

	private GameObject _field;

	private float _curTimeCount;
	private bool _lerpToNormalTime;
	private bool _hasResetTime = true;
	public float ReturnToNormalStep;

	public GameObject JockeySplit;

	public Material GrayScaleMaterial;
	public Material DefaultMaterial;
	private GameObject[] _grayscaleExcluded;
	private Color _grayscaleStartColorTint;
	private float _grayscaleStartAlpha;

	public GameObject CorpseObject;
	// Use this for initialization
	void Start () {
		_field = GameObject.Find ("Field");
		_grayscaleStartColorTint = GrayScaleMaterial.GetColor("_Color");
		Debug.Log ("Color:" + _grayscaleStartColorTint);
		_grayscaleStartAlpha = _grayscaleStartColorTint.a;
	}
	
	// Update is called once per frame
	void Update () {
		//_grayscaleStartColorTint = GrayScaleMaterial.GetColor("_Color");
		TimeUpdate ();
	}

	public void SpawnParticles(int particleNumber, Vector3 particleDirection, float angleVariation, Vector3 spawnPosition)
	{
		//Debug.Break ();
		for(int i = 0; i<particleNumber; i++)
		{
			GameObject newParticle = (GameObject)Instantiate(BloodParticle, spawnPosition, transform.rotation);
			newParticle.GetComponent<FakeParticleBlood>().SetupParticle(particleDirection+new Vector3(Random.Range(-angleVariation, angleVariation), Random.Range(-angleVariation, angleVariation), 0f),
			                                                            Random.Range(1f, 3f), 0.075f, 5f);
		}
	}

	public GameObject FillScreenColor(bool fade, float time, Color col, bool destroy, string sortingLayer, int sortPos)
	{
		GameObject newFill = (GameObject)Instantiate (ScreenFill);
		newFill.GetComponent<EffectScreenColorFill> ().SetupFill (fade, time, col, destroy, sortingLayer, sortPos);
		return newFill;
	}

	public void SpawnCorpse(Vector3 spawnPos, Vector3 spawnDirection, float spawnVariation)
	{
		GameObject newCorpse = (GameObject)Instantiate (CorpseObject, spawnPos, transform.rotation);
		Vector3 spawn = spawnDirection;
		spawn = UtilityFunctions.RotateVector2 (spawnVariation, (Vector2)spawnDirection);
		newCorpse.GetComponent<EffectCorpse>().SetupCorpse(spawn, 300f);
	}

	public void CreateText(Vector3 createPosition, Color textColor, float textLife, bool flashMust, string textDisplayed, int fontSize, float textSpeed){
		GameObject newText = (GameObject)(Instantiate(EffectText, createPosition, transform.rotation));
		Effect_Text text = newText.GetComponent<Effect_Text> ();
		text.Setup (textColor, textLife, textDisplayed, true, textSpeed, false, flashMust, fontSize);
	}

	public void TimeUpdate()
	{
		if(!_hasResetTime){
			if(_curTimeCount <= 0){
				Time.timeScale = _lerpToNormalTime ? Mathf.Lerp(Time.timeScale, 1f, ReturnToNormalStep): 1f;
				UpdateGrayscaleWorld();
				if(Time.timeScale >= 0.98){
					Time.timeScale = 1f;
					ResetWorldColor();
					_hasResetTime = true;
				}
			}else{
				_curTimeCount -= Time.unscaledDeltaTime;
			}
		}
	}

	public void AffectTime(float timeGoal, float newTimePeriod, bool lerpBack)
	{
		//Camera.main.gameObject.GetComponent<CameraLocalMultiplayerBehavior> ().ShakeEnd ();
		_curTimeCount = newTimePeriod;
		Time.timeScale = timeGoal;
		_lerpToNormalTime = lerpBack;
		_hasResetTime = false;
	}

	public void GrayscaleWorld(GameObject[] excluded)
	{
		_grayscaleExcluded = excluded;
		//SetWorldMaterial (GrayScaleMaterial);
		SetSpecificObjectsMaterial (excluded, DefaultMaterial);
	}
	
	public void UpdateGrayscaleWorld(){

		float timeScaleMapped = UtilityFunctions.Map (Time.timeScale, 0.2f, 1f, 0f, 1f);
		Color curCol = Color.Lerp (_grayscaleStartColorTint, Color.white, timeScaleMapped);
		GrayScaleMaterial.SetColor ("_Color", curCol);
		GrayScaleMaterial.SetFloat ("Effect Amount", timeScaleMapped);

		//SetWorldMaterial (GrayScaleMaterial);
		SetSpecificObjectsMaterial (_grayscaleExcluded, DefaultMaterial);
	}

	public void ResetWorldColor(){
		//SetWorldMaterial (DefaultMaterial);
	}

	private void SetSpecificObjectsMaterial(GameObject[] targets, Material worldMaterial){

		foreach(GameObject g in targets){
			SpriteRenderer curRender = g.GetComponent<SpriteRenderer>();
			curRender.material = worldMaterial;

			SpriteRenderer[] renders = g.GetComponentsInChildren<SpriteRenderer>();
			foreach(SpriteRenderer s in renders){
				s.material = worldMaterial;
			}
		}
	}

	/*private void SetWorldMaterial(Material worldMaterial)
	{
		List<GameObject> worldTiles;
			//_field.GetComponent<FieldGenerator> ().GetTileList ();
		GameObject[] orcs = GameObject.FindGameObjectsWithTag ("Orc");
		GameObject[] raptors = GameObject.FindGameObjectsWithTag ("Raptor");
		SpriteRenderer[] goalRenders = GameObject.Find ("Goals").GetComponentsInChildren<SpriteRenderer> ();
		
		foreach (GameObject g in worldTiles) {
			SpriteRenderer curRender = g.GetComponent<SpriteRenderer>(); 
			curRender.material = worldMaterial;
			//curRender.material.SetColor("_Color", colorTint);
		}
		
		foreach (GameObject g in orcs) {
			SpriteRenderer curRender = g.GetComponent<SpriteRenderer>(); 
			curRender.material = worldMaterial;
			//curRender.material.SetColor("_Color", colorTint);
			SpriteRenderer[] renders = g.GetComponentsInChildren<SpriteRenderer>();
			foreach(SpriteRenderer s in renders){
				s.material = worldMaterial;
				//s.material.SetColor("_Color", colorTint);
			}
		}
		
		foreach(SpriteRenderer s in goalRenders){
			s.material = worldMaterial;
			//s.material.SetColor("_Color", colorTint);
		}
		
		foreach (GameObject g in raptors) {
			SpriteRenderer curRender = g.GetComponent<SpriteRenderer>(); 
			curRender.material = worldMaterial;
			//curRender.material.SetColor("_Color", colorTint);
			SpriteRenderer[] renders = g.GetComponentsInChildren<SpriteRenderer>();
			foreach(SpriteRenderer s in renders){
				s.material = worldMaterial;
				//s.material.SetColor("_Color", colorTint);
			}
		}	
	}*/
}
