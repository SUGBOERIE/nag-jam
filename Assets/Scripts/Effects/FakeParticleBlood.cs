﻿using UnityEngine;
using System.Collections;

public class FakeParticleBlood : MonoBehaviour {

	private Vector2 _currentDirection;
	private float _currentSpeed;
	private float _currentFriction;
	private float _currentTime;

	private GameObject _field;

	private Vector2 _scaleRange;

	private bool _mustFade;

	void Start()
	{
		transform.Rotate (new Vector3(0f, 0f, Random.Range(0f, 360f)));
	}
	
	// Update is called once per frame
	void Update () {
		if (Mathf.Abs (_currentSpeed) <= _currentFriction || Mathf.Abs(_currentSpeed) < 0.1f) {
			_currentSpeed = 0f;
		} else {
			if(_currentSpeed > 0f){
				_currentSpeed -= _currentFriction*Time.timeScale; 
			}else if(_currentSpeed < 0f){
				_currentSpeed += _currentFriction*Time.timeScale;
			}
		}
		transform.position += (Vector3)(_currentDirection*(_currentSpeed*Time.timeScale));

		if(_currentTime <= 0){
			_mustFade = true;
		}else 
		{
			_currentTime -= Time.deltaTime;
		}

		if(_mustFade){
			Color curCol = GetComponent<SpriteRenderer>().color;
			curCol.a = Mathf.Lerp(curCol.a, 0f, 0.075f);
			if(curCol.a < 0.05) Destroy(gameObject);
			GetComponent<SpriteRenderer>().color = curCol;
		}

	}


	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.tag == "Field") 
		{
			_currentSpeed = 0f;
		}
	}

	public void SetupParticle(Vector2 direction, float startSpeed, float friction, float time)
	{
		_currentDirection = direction;
		_currentSpeed = startSpeed;
		_currentFriction = friction;
		_currentTime = time;
		transform.rotation = new Quaternion(0f, 0f, Random.Range(0f, 360f), 0f);
		_field = GameObject.Find ("Field");

		_scaleRange = new Vector2 (3f, 10f);
		float scale = Random.Range (_scaleRange.x*1f, _scaleRange.y*1f);
		transform.localScale = Vector3.one * scale;
	}
}
