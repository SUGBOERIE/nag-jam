﻿using UnityEngine;
using System.Collections;

public class Effect_Text : MonoBehaviour {

	public Color textColour;
	public float textLife;

	public string textDrawn;

	public bool mustFloat;
	public float floatSpeed;

	public bool mustFade;
	public float fadeSpeed;

	private TextMesh textDrawer;
	private float currentTime;

	private bool _mustFlash;
	private bool _startFlash;
	public float FlashPeriod; 

	public string RenderLayer;

	private bool _isColor = true;
	// Use this for initialization
	public void Setup (Color tempColour, float tempLife, string tempString, bool floatUp, 
	            float floatingSpeed, bool fadeMust, bool flashMust, int fontSize) {
		transform.rotation = new Quaternion(0,0,0,0);

		textColour = tempColour;
		textColour.a = 1;
		textLife = tempLife;
		textDrawn = tempString;

		mustFloat = floatUp;
		floatSpeed = floatingSpeed;

		mustFade = fadeMust;

		textDrawer = GetComponent<TextMesh>();
		textDrawer.text = textDrawn;
		textDrawer.color = tempColour;
		textDrawer.fontSize = fontSize;
		currentTime = textLife;
		_mustFlash = flashMust;
		GetComponent<Renderer> ().sortingLayerName = RenderLayer;
		if(mustFade)fadeSpeed = 1/textLife;
	}
	
	// Update is called once per frame
	void Update () {
		if(mustFloat) transform.Translate(Vector3.up * floatSpeed);
		if(mustFade){
			textColour.a = currentTime/textLife;
			textDrawer.GetComponent<Renderer>().material.color = textColour;
		}

		if(currentTime >= 0){
			currentTime-= Time.deltaTime;
		}else{
			if (_mustFlash){
				if(!_startFlash)StartCoroutine(FlashAlternate(FlashPeriod));
				mustFloat = false;
				_startFlash = true;
			}else{
				GameObject.Destroy(gameObject);
			}	
		}
	}

	IEnumerator FlashAlternate(float period)
	{
		yield return new WaitForSeconds (period);
	
		Color curColor = textDrawer.color;
		if(_isColor){
			curColor = Color.white;
			_isColor = false;
		}else{
			_isColor = true;
			curColor = textColour;
		}
		textDrawer.color = curColor; 
		FlashPeriod*= 0.95f;
		if (FlashPeriod <= 0.03f)
			Destroy (gameObject);

		StartCoroutine(FlashAlternate(FlashPeriod));
	}
}
