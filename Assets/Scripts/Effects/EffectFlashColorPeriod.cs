﻿using UnityEngine;
using System.Collections;

public class EffectFlashColorPeriod : MonoBehaviour {
	private Material FlashMaterial;
	public float FlashTime;
	private float _curTime = -10;
	private Renderer _render;
	private Material _previousMaterial;
	public bool DestroyObjectAfterFlash;
	// Use this for initialization
	void Start () {
		_render = GetComponent<Renderer> ();
		//_previousMaterial = _render.material;
		//_render.material = FlashMaterial;
		//_curTime = FlashTime;
	}

	public void SetupFlash(float time, bool destroyAfter, Material flashMat)
	{
		_render = GetComponent<Renderer> ();
		_previousMaterial = _render.material;
		_curTime = time;
		DestroyObjectAfterFlash = destroyAfter;
		_render.material = flashMat;
		Debug.Log (_previousMaterial);
	}
	
	// Update is called once per frame
	void Update () {
		if (_curTime != -10) {
			if (_curTime <= 0) {
				ReturnToNormal ();
			} else {
				_curTime -= Time.deltaTime;
			}
		}
	}

	private void ReturnToNormal()
	{
		_render.material = _previousMaterial;

		if(DestroyObjectAfterFlash){
			Destroy(gameObject);
		}else{
			Destroy (this);
		}
	}
}
