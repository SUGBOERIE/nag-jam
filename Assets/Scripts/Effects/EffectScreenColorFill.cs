﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Plane))]
public class EffectScreenColorFill : MonoBehaviour {

	public bool FadeOut;
	public float FadeTime;
	private float _startFadeTime;

	public Color FillColor;

	private SpriteRenderer _render;

	private bool _isSetup;
	private bool _destroyOnArrival;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void LateUpdate () {
		if(_isSetup) ColorFade ();

		transform.position = Camera.main.transform.position+ new Vector3(0f, 0f, 10f);
	}

	public void SetupFill(bool fadeToColor, float fadeTime, Color fillColor, bool destroy, string sortLayer, int sortPos)
	{
		FadeOut = fadeToColor;
		FadeTime = fadeTime;
		FillColor = fillColor;
		_render = gameObject.GetComponent<SpriteRenderer> ();
		_render.sortingLayerName = sortLayer;
		_render.sortingOrder = sortPos;
		SetupColorPlane ();
		_isSetup = true;
		_destroyOnArrival = destroy;

	}

	private void SetupColorPlane()
	{
		Color col = FillColor;

		if(FadeOut)
		{
			col.a = 1; 
		}else {
			col.a = 0;
		}
		_render.color = col;

		float maxCamSize;

		if(Camera.main.gameObject.GetComponent<TopDownPlayerCamera> () != null){
			maxCamSize = Camera.main.orthographicSize;
		}else{
			maxCamSize = Camera.main.orthographicSize;
		}

		transform.localScale = new Vector3 ((maxCamSize*5f)*Camera.main.aspect, maxCamSize*5f, 0f);

		
		_startFadeTime = FadeTime;
		FadeTime = FadeOut ? FadeTime : 0;
	}

	private void ColorFade()
	{
		if(_render == null)
			_render = GetComponent<SpriteRenderer> ();

		Color curCol = FillColor;
		FadeTime += (FadeOut ? -Time.deltaTime : Time.deltaTime);
		FadeTime = Mathf.Clamp (FadeTime, 0, _startFadeTime);
		curCol.a = (FadeTime / _startFadeTime);
		_render.color = curCol;
		if ((FadeTime >= _startFadeTime || FadeTime <= 0) && _destroyOnArrival)
			Destroy (gameObject);
	}
}
