﻿using UnityEngine;
using System.Collections;
using InControl;

public class WeaponParent : MonoBehaviour
{

    public bool CanFire;

    private GameObject _manager;

    public WeaponType WeaponTypeInstance;
	public AmmoType AmmoTypeInstance;


    public int Ammunition;
    private float _disperseVariance;
    private GameObject _bulletType;
    private int _bulletSpawnNo;
    private TopDownPlayerCamera _camera;
	private float _damageModifier;

    public Vector2 AimDirection
    {
        get
        {
            return (UtilityFunctions.RotateVector2(transform.parent.rotation.eulerAngles.z, Vector2.right));
        }
    }

    public InputDevice _controller
    {
        get
        {
            return transform.parent.gameObject.GetComponent<CrossHairMovement>()._controller;
        }
    }

    //Rate of Fire:
    private float _rateOfFirePerSecond;
    //Shots per second
    private float _rateOfFireAcutal;
    //In time between shots
    private float _curRateTime;

    // Use this for initialization
    void Start()
    {
        _manager = GameObject.Find("GameManager");
        SetupWeaponProperties();
    }
	
    // Update is called once per frame
    void Update()
    {
        ManageWeapon();
    }

    private void ManageWeapon()
    {
         if (CanFire && _controller.RightTrigger.Value > 0.5f)
         {
             if (Ammunition > 0)
             {
                 FireWeapon(_bulletSpawnNo);
             }
             else
             { //Out of ammo

             }
         }
         ManageRateOfFire();
    }

    private void FireWeapon(int bulletsNo)
    {
        CanFire = false;
        _curRateTime = _rateOfFireAcutal;
		if (bulletsNo > 1) {
			float spread = 90f;
			float iteration = spread/bulletsNo;
			Vector2 aimModified = AimDirection;
			aimModified = UtilityFunctions.RotateVector2(-spread/2f, AimDirection);
			for (var i = 0; i < bulletsNo; i++) {
				float shootAngleVariance = Random.Range (-_disperseVariance, _disperseVariance); 
				Vector2 bulletDirection = UtilityFunctions.RotateVector2 (shootAngleVariance, aimModified);  
				bulletDirection= UtilityFunctions.RotateVector2 (iteration*i, aimModified);  

				
				GameObject Bullet = (GameObject)Instantiate (_bulletType, transform.position, transform.rotation);
				var bParent = Bullet.GetComponent<BulletParent> ();
				bParent.BulletSetup (bulletDirection, transform.parent.rotation.eulerAngles.z, _damageModifier);
			}
			Ammunition--;
		} else {
			float shootAngleVariance = Random.Range(-_disperseVariance, _disperseVariance); 
			Vector2 bulletDirection = UtilityFunctions.RotateVector2(shootAngleVariance, AimDirection);  
			Ammunition--;
			
			GameObject Bullet = (GameObject)Instantiate(_bulletType, transform.position, transform.rotation);
			var bParent = Bullet.GetComponent<BulletParent>();
			bParent.BulletSetup(bulletDirection, transform.parent.rotation.eulerAngles.z, _damageModifier);
		}
        
    }

    private void ManageRateOfFire()
    {
        if (_curRateTime <= 0)
        {
            CanFire = true;
        }
        else
        {
            _curRateTime -= Time.deltaTime;
        }
    }

    private void SetupWeaponProperties()
    {
        
        switch (WeaponTypeInstance)
        {
            case WeaponType.Pistol:
                SetupWeapon(GetBulletType(), 3, 30, 3, 1, 0.8f);
            break;

            case WeaponType.Rifle:
			SetupWeapon(GetBulletType(), 1, 15, 5, 1, 1.2f);
            break;

            case WeaponType.Shotgun:
			SetupWeapon(GetBulletType(), 1, 10, 3, 8, 0.5f);
            break;

            case WeaponType.Automatic:
			SetupWeapon(GetBulletType(), 5, 100, 5, 1, 0.3f);
            break;

			case WeaponType.Explosion:
			SetupWeapon(GetBulletType(), 5, 100, 5, 1, 0.7f);
			break;

			case WeaponType.Laser:
			SetupWeapon(GetBulletType(), 5, 100, 5, 1, 1f);
			break;

			case WeaponType.SoundWave:
			SetupWeapon(GetBulletType(), 5, 100, 5, 1, 1f);
			break;
        }
        _rateOfFireAcutal = 1f / _rateOfFirePerSecond;
    }

	private GameObject GetBulletType()
	{
		BulletManager _bulletManager = GameManager.Instance.b;
		switch (AmmoTypeInstance) {
			case AmmoType.Normal:
			return _bulletManager.BulletTypes[0];
			break;

			case AmmoType.Concussion:
			return _bulletManager.BulletTypes[1];
			break;

			case AmmoType.Confusion:
			return _bulletManager.BulletTypes[2];
			break;

			case AmmoType.Exploding:
			return _bulletManager.BulletTypes[3];
			break;

			case AmmoType.Incendiary:
			return _bulletManager.BulletTypes[4];
			break;

			case AmmoType.Piercing:
			return _bulletManager.BulletTypes[5];
			break;
		}
		return _bulletManager.BulletTypes[0];
	}

    private void SetupWeapon(GameObject bulletType, int RateOfFire, int startingAmmo, int disperseVariance, int spawnNo, float damageMod)
    {
        _bulletType = bulletType;
        _rateOfFirePerSecond = RateOfFire;
        Ammunition = startingAmmo;
        _disperseVariance = disperseVariance;
        _bulletSpawnNo = spawnNo;
		_damageModifier = damageMod;
    }
}
