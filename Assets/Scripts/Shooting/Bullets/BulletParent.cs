﻿using UnityEngine;
using System.Collections.Generic;

public class BulletParent : MonoBehaviour
{

    public float StartSpeed;
    private float _speed;
    private Vector2 _moveDirection;
    public int Friction;

    public int Damage;
    public bool DestroyOnEnemyCollision;
    public string[] CollisionTags;


	private List<GameObject> HitObjects = new List<GameObject>();


    protected TopDownPlayerCamera _camera;
	protected EffectManager _effects;

    enum BulletCollisionType
    {
        Enemy,
        Wall,
        None}

    ;

    //Properties
    private Rigidbody2D _mover;

    void Start()
    {
        _camera = Camera.main.GetComponent<TopDownPlayerCamera>();
    }

    private void SetupProperties()
    {
		_effects = GameManager.Instance.GetComponent<EffectManager> ();
        _mover = GetComponent<Rigidbody2D>();
    }

    public void BulletSetup(Vector2 shootDirection, float faceAngle, float damageMod)
    {
        SetupProperties();
        _speed = StartSpeed;
        _moveDirection = shootDirection;
        Vector2 dir = shootDirection;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		Damage = Mathf.RoundToInt(Damage*damageMod);
    }
	
    // Update is called once per frame
    void Update()
    {
        _speed = Mathf.Clamp(_speed - Friction, 0f, StartSpeed);
        _mover.velocity = _moveDirection * _speed;
        if (_mover.velocity.magnitude < 0.5f)
            Destroy(gameObject);
    }

    private void BulletCollision(BulletCollisionType bullColl, GameObject collidingInstance)
    {
        switch (bullColl)
        {
            case BulletCollisionType.Wall:
                collidingInstance.GetComponent<HealthManager>().DamageInstance(Damage, _moveDirection);
                break;

            case BulletCollisionType.Enemy:
				EnemyCollision(collidingInstance);
                break;
        }

        if (DestroyOnEnemyCollision)
            Destroy(gameObject);
    }

    private BulletCollisionType GetCollisionType(string collisionTag)
    {
        switch (collisionTag)
        {
            case "Enemy":
                return BulletCollisionType.Enemy;

            case "Terrain":
                return BulletCollisionType.Wall;
        }

        return BulletCollisionType.None;
    }

	public virtual void EnemyCollision(GameObject collidingInstance)
	{
		collidingInstance.GetComponent<HealthManager>().DamageInstance(Damage, _moveDirection);
		HitObjects.Add (collidingInstance);
		_effects.AffectTime (0f, .002f, false);
		_camera.ShakeScreen(new Vector3(5f, 5f), 0.1f, false);

	}

	private bool CheckNoDamageDealt(GameObject checkingObject)
	{
		for(var i = 0; i < HitObjects.Count; i++)
		{
			if(checkingObject.GetInstanceID() == HitObjects[i].GetInstanceID())
			{
				return true;
			}
		}
		return false;
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        for (var i = 0; i < CollisionTags.Length; i++)
        {
            if (coll.tag == CollisionTags[i] && !CheckNoDamageDealt(coll.gameObject))
            {
                BulletCollision(GetCollisionType(CollisionTags[i]), coll.gameObject);
                break;
            }
        }
    }
}
