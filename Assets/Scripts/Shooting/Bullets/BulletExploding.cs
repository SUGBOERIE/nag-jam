﻿using UnityEngine;
using System.Collections;

public class BulletExploding : BulletParent {
	
	public GameObject SmallExplosion;

	public override void EnemyCollision(GameObject collidingInstance)
	{
		Instantiate (SmallExplosion, collidingInstance.transform.position, transform.rotation);
		_effects.AffectTime (0f, .002f, false);
		_camera.ShakeScreen (new Vector3 (10f, 10f), 0.2f, false);
	}
}
