﻿using UnityEngine;
using System.Collections;

public class FloorMaker : MonoBehaviour {

	public float TimeBetweenSteps; 
	private float _curTime = -10;

	public GameObject FloorObject;
	private int[] _rotationOptions;
	private float[] _rotationChances;
	private int _totalTiles; 
	private float _defaultFloorMakerChance;
	private float _1by1Chance;
	private float _2by2Chance;
	private float _3by3Chance;
	private float _5by5Chance;
	private LevelGenerator _levelGenerator;

	public GameObject TileMaker;

	public float TileSize;
	// Use this for initialization
	void Start () {

	}

	public void SetupFloorMaker(int[] rotationOptions, int totalTiles, float floorMakerChance, float twoby2chance, 
	                            float threeBy3chance, LevelGenerator generator, float[] rotationChances, float fiveByFiveChance, float oneChance)
	{
		_rotationOptions = rotationOptions;
		_totalTiles = totalTiles;
		_defaultFloorMakerChance = floorMakerChance;
		_1by1Chance = oneChance;
		_2by2Chance = twoby2chance;
		_3by3Chance = threeBy3chance;
		_5by5Chance = fiveByFiveChance;
		_curTime = TimeBetweenSteps;
		_levelGenerator = generator;
		_rotationChances = rotationChances;
		transform.rotation = new Quaternion(0f, 0f, GetRotation (), 0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(_curTime != -10){
			if (_curTime <= 0) {
				Step();
				_curTime = TimeBetweenSteps;
			} else {
				_curTime -= Time.deltaTime;
			}
		}
	}

	private void Step()
	{
		if (_levelGenerator.Tiles.Count >= _totalTiles)
			Destroy (gameObject);
		GenerateTile ();
		GenerateTileMaker ();


		transform.Rotate(new Vector3(0f, 0f, GetRotation()));
	}

	private void GenerateTileMaker()
	{
		float random = Random.Range (0, 100);
		int makerNo = GameObject.FindGameObjectsWithTag ("FloorMaker").Length;
		float floorMakerChance = (_defaultFloorMakerChance*100)-(makerNo*2);
		floorMakerChance = Mathf.Clamp (floorMakerChance, 0, 100);
		if (random < floorMakerChance) 
		{
			GameObject newTile = (GameObject)Instantiate(TileMaker, transform.position, new Quaternion(0f, 0f, transform.rotation.z+GetRotation(), 0f));
			newTile.GetComponent<FloorMaker>().SetupFloorMaker(_rotationOptions, _totalTiles, _defaultFloorMakerChance, _2by2Chance, _3by3Chance, _levelGenerator, _rotationChances, _5by5Chance, _1by1Chance);
			newTile.transform.position += TileSize*transform.up;
		}

	}

	private void GenerateTile()
	{
		float random = Random.Range (0, 100);
		float chance1 = (_2by2Chance*100);//2x2
		float chance2 = chance1+(_3by3Chance*100);//3x3
		float chance3 = chance2+(_5by5Chance*100);//5
		float chance4 = chance3+(_1by1Chance*100);//Single

		Debug.Log ("Chance1: " + chance1);
		Debug.Log ("Chance2: " + chance2);
		Debug.Log ("Chance3: " + chance3);
		Debug.Log ("Chance4: " + chance4);

		if(random<= chance1){
			GenerateTilesVariable(2, 2);
		}else if(random > chance1 && random <chance2){
			GenerateTilesVariable(3, 3);
		}else if(random >= chance2 && random <chance3){
			GenerateTilesVariable(5, 5);
		}else if(random >= chance3 && random <chance4){
			GenerateSingleTile();
		}
	}

	private void GenerateSingleTile()
	{
		Quaternion zero = new Quaternion (0f, 0f, 0f, 0f);
		//for (var i= 0; i <2; i++) {
			if (!TilePositionExists (transform.position)) {
				GameObject newTile = (GameObject)Instantiate (FloorObject, transform.position, zero);
				newTile.transform.parent = GameObject.Find ("Floors").transform;
				_levelGenerator.Tiles.Add (newTile);
			}

		if (!TilePositionExists (transform.position+TileSize*transform.right)) {
				GameObject newTile = (GameObject)Instantiate (FloorObject, transform.position+(TileSize*transform.right), zero);
				newTile.transform.parent = GameObject.Find ("Floors").transform;
				_levelGenerator.Tiles.Add (newTile);
			}
		//}
		transform.position += TileSize * transform.up;
	}

	private void GenerateTilesVariable(int xNo, int yNo)
	{
		Quaternion zero = new Quaternion (0f, 0f, 0f, 0f);
		Vector3 offset = Vector3.zero;
		for (int i = 0; i <xNo; i++) {
			for(int j = 0; j <yNo; j++){
				offset = new Vector3(i*TileSize, j*TileSize, 0f);
				if(!TilePositionExists(transform.position+offset)){
					GameObject newTile = (GameObject)Instantiate (FloorObject, transform.position+offset, zero);
					newTile.transform.parent = GameObject.Find ("Floors").transform;
					_levelGenerator.Tiles.Add (newTile);
				}
			}
		}
		transform.position += (TileSize * transform.up);
	}
	/*
	private void Generate2By2Tiles()
	{
		Quaternion zero = new Quaternion (0f, 0f, 0f, 0f);
		Vector3 offset = Vector3.zero;
		for (int i = 0; i <2; i++) {
			for(int j = 0; j <2; j++){
				offset = new Vector3(i*TileSize, j*TileSize, 0f);
				if(!TilePositionExists(transform.position+offset)){
					GameObject newTile = (GameObject)Instantiate (FloorObject, transform.position+offset, zero);
					newTile.transform.parent = GameObject.Find ("Terrain").transform;
					_levelGenerator.Tiles.Add (newTile);
				}
			}

		}
		transform.position += offset+(TileSize * transform.up);
	}

	private void Generate3by3Tiles()
	{
		Quaternion zero = new Quaternion (0f, 0f, 0f, 0f);
		Vector3 offset = Vector3.zero;
		for (int i = 0; i <3; i++) {
			for(int j = 0; j <3; j++){
				offset = new Vector3((i*TileSize)*transform.up, (j*TileSize)*transform.up, 0f);
				if(!TilePositionExists(transform.position+offset)){
					GameObject newTile = (GameObject)Instantiate (FloorObject, transform.position+offset, zero);
					newTile.transform.parent = GameObject.Find ("Terrain").transform;
					_levelGenerator.Tiles.Add (newTile);
				}
			}
		}
		transform.position += offset+(TileSize * transform.up);
	}

	private void Generate5by5Tiles()
	{
		Quaternion zero = new Quaternion (0f, 0f, 0f, 0f);
		Vector3 offset = Vector3.zero;
		for (int i = 0; i <5; i++) {
			for(int j = 0; j <5; j++){
				offset = new Vector3(i*TileSize, j*TileSize, 0f);
				if(!TilePositionExists(transform.position+offset)){
					GameObject newTile = (GameObject)Instantiate (FloorObject, transform.position+offset, zero);
					newTile.transform.parent = GameObject.Find ("Terrain").transform;
					_levelGenerator.Tiles.Add (newTile);
				}
			}
		}
		transform.position += offset+(TileSize * transform.up);
	}*/

	private bool TilePositionExists(Vector3 searchPos)
	{
		for(var i = 0; i < _levelGenerator.Tiles.Count; i++)
		{
			if(searchPos == _levelGenerator.Tiles[i].transform.position){
				return true;
			}
		}
		
		return false;
	}

	private float GetRotation()
	{
		float random = Random.Range (0, 100);
		float chance1 = (_rotationChances [0] * 100); 
		float chance2 = chance1+(_rotationChances [1] * 100); 
		float chance3 = chance2+(_rotationChances [2] * 100); 
		float chance4 = chance3+(_rotationChances [3] * 100); 

		if(random <=chance1){
			return _rotationOptions[0];
		}else if(random >chance1 && random <=chance2){
			return _rotationOptions[1];
		}else if(random >chance2 && random <=chance3){
			return _rotationOptions[2];
		}else if(random >chance3 && random <=chance4){
			return _rotationOptions[3];
		}

		return 0;
	}
}
