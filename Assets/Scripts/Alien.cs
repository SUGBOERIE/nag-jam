﻿using UnityEngine;
using System.Collections;
using InControl;

public class Alien : MonoBehaviour
{
    private AlienHorde _horde;
	public GameObject CorpseObject;
	public EffectManager _effects;
    void Start()
    {
        _horde = GetComponentInParent<AlienHorde>();
		_effects = GameObject.Find ("GameManager").GetComponent<EffectManager> ();
    }
	
    void Update()
    {
	
    }

    void OnDestroy()
    {
		//_effects.SpawnCorpse (transform.position, GetComponent<HealthManager> ().GetDeathDirection (), 15f);
    	_horde.DestroyAlien(gameObject);
    }

    public void TurnPlayer(GameObject player, InputDevice i)
    {
        _horde.ChangeState(AlienHordeState.PLAYER, i);
        Destroy(player);
    }
}
