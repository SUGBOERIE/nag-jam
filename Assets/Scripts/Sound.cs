﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class Sound : MonoBehaviour
{

    public AudioSource audioOneShotPrefab;

    public static Sound Instance;

    void Awake()
    {
        Instance = this;
    }


    public static void PlaySoundAt(AudioClip clip, Vector2 pos, float vol)
    {
        var audio = Instantiate(Instance.audioOneShotPrefab, pos, Quaternion.identity) as AudioSource;

        //audio.outputAudioMixerGroup = DynMix;
        audio.clip = clip;
        audio.pitch = Random.Range(0.7f, 1.3f);
        audio.volume = vol;

        audio.Play();
        Destroy(audio.gameObject, clip.length * 2f);

    }

    public static void PlaySoundEffectAt(AudioClip[] audioClip, Vector2 pos, float vol)
    {
        var audio = Instantiate(Instance.audioOneShotPrefab, pos, Quaternion.identity) as AudioSource;
        var clip = audioClip[Random.Range(0, audioClip.Length)];

        audio.clip = clip;
        audio.pitch = Random.Range(0.7f, 1.3f);
        audio.volume = vol;



        audio.Play();
        Destroy(audio.gameObject, clip.length * 2f);

    }

    public static void PlaySoundEffectAt(AudioClip[] audioClip, Vector2 pos, float vol, Vector2 pitchRange)
    {
        var audio = Instantiate(Instance.audioOneShotPrefab, pos, Quaternion.identity) as AudioSource;
        var clip = audioClip[Random.Range(0, audioClip.Length)];

        audio.clip = clip;
        audio.pitch = Random.Range(pitchRange.x, pitchRange.y);
        audio.volume = vol;

        audio.Play();
        Destroy(audio.gameObject, clip.length * 2f);

    }

    public static void PlaySoundEffectAt(AudioClip[] audioClip, Vector2 pos, float vol, float pitch)
    {
        var audio = Instantiate(Instance.audioOneShotPrefab, pos, Quaternion.identity) as AudioSource;
        var clip = audioClip[Random.Range(0, audioClip.Length)];

        audio.clip = clip;
        audio.pitch = pitch;
        audio.volume = vol;

        audio.Play();
        Destroy(audio.gameObject, clip.length * 2f);

    }

    public static void PlaySoundLoopEffectAt(AudioClip[] audioClip, Vector2 pos, AudioMixerGroup DynMix)
    {
        var audio = Instantiate(Instance.audioOneShotPrefab, pos, Quaternion.identity) as AudioSource;
        var clip = audioClip[Random.Range(0, audioClip.Length)];

        audio.outputAudioMixerGroup = DynMix;
        audio.loop = true;
        audio.clip = clip;

        audio.Play();
        Destroy(audio.gameObject, clip.length * 2f);

    }

}
