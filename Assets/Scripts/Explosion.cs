﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	public float AlternateTime;
	private float _currentTime;

	public float ExistTime;
	private float _existTime;

	private SpriteRenderer _render;
	private bool isWhite = true;

	public int Damage;
	private EffectManager _effects;

	// Use this for initialization
	void Start () {
		_currentTime = AlternateTime;
		_render = GetComponent<SpriteRenderer> ();
		_existTime = ExistTime;

	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.tag == "Player" || coll.tag == "Enemy") {
			if(coll.tag == "Enemy")coll.GetComponent<HealthManager>().DamageInstance(Damage, (coll.transform.position-transform.position).normalized);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(_currentTime <= 0)
		{
			_render.material.color =  isWhite? Color.black: Color.white ;
			isWhite = !isWhite;
			_currentTime = AlternateTime;
		}else{
			_currentTime -= Time.deltaTime;
		}

		if(_existTime <= 0)
		{
			Destroy(gameObject);
		}else{
			_existTime -= Time.deltaTime;
		}
	}
}
