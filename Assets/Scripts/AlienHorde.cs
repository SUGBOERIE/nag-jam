using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Gamelogic;
using InControl;
using UnityEngine.UI;

public class AlienHorde : MonoBehaviour
{
    // Horde Properties
    public int NumAliens;
    public GameObject Alien;
    public float AttackRadius = 100f;
    public LayerMask SightLayer;

    // Alien Properties
    public float SeparationRadius = 0.3f;

    public Text DebugText;

    private List<AlienMovement> _aliens = new List<AlienMovement>();
    private Vector3 _hordeLeader;
    private GameObject _playerTarget;
    private StateMachine<AlienHordeState> _stateMachine;
    private InputDevice _playerController;

    void Start()
    {
        GenerateHorde(NumAliens);
        GetComponentsInChildren(_aliens);
        _hordeLeader = _aliens[0].transform.position;

        _stateMachine = new StateMachine<AlienHordeState>();
        _stateMachine.AddState(AlienHordeState.AI, null, UpdateUI, null);
        _stateMachine.AddState(AlienHordeState.PLAYER, null, UpdatePayer, null);
        _stateMachine.ChangeState(AlienHordeState.AI);
    }
	
    void Update()
    {
        _stateMachine.Update();
        _hordeLeader = _aliens[0].transform.position;
    }

    void UpdateUI()
    {
        CheckAttackRadius();

        if (_playerTarget != null)
            SetSeekPosition(_playerTarget.transform.position);
    }

    void UpdatePayer()
    {
        var s = _aliens[0].transform.position + (Vector3)(_playerController.LeftStick.Vector * 100f);
        SetSeekPosition(s);
    }

    private void GenerateHorde(int numAliens)
    {
        var spawnRadius = numAliens * 32f * 0.5f; // 32f is current size of aliens, this should be variable

        for (int i = 0; i < numAliens; i++)
        {
            var pos = transform.position + (Vector3)(Random.insideUnitCircle * spawnRadius);
            var alien = Instantiate(Alien, pos, Quaternion.identity) as GameObject;
            alien.transform.SetParent(transform);
        }
    }

    private void SetSeekPosition(Vector3 v)
    {
    	UtilityFunctions.DebugDrawCircle(v, 15f, Color.white);

        foreach (var a in _aliens)
        {
            a.SeekPosition = v;
        }
    }

    public void CheckAttackRadius()
    {
        UtilityFunctions.DebugDrawCircle(_hordeLeader, AttackRadius, Color.green);

        for (int i = 0; i < 360; i++)
        {
            var dir = (Vector3)UtilityFunctions.RotateVector2(i, Vector2.right);
            var hit = Physics2D.Raycast(_hordeLeader, dir, AttackRadius, SightLayer);

            var color = Color.magenta;
            color.a = 0.1f;

            if (hit)
            {
                if (hit.collider.gameObject.tag == "Player" && _playerTarget == null)
                    _playerTarget = hit.collider.gameObject;

                Debug.DrawLine(_hordeLeader, hit.point, color);
            }
            else
            {
                var end = _hordeLeader + dir * AttackRadius;
                Debug.DrawLine(_hordeLeader, end, color);
            }

        }
    }

    public void DestroyAlien(GameObject alien)
    {
        _aliens.Remove(alien.GetComponent<AlienMovement>());
            
        Destroy(alien);

        if (_aliens.Count == 0)
            Destroy(gameObject);
    }

    public void ChangeState(AlienHordeState state, InputDevice device)
    {
    	switch (state)
    	{
    		case AlienHordeState.PLAYER:
    			_playerController = device;
    			break;
    	}

    	_stateMachine.ChangeState(state);
    }

}
