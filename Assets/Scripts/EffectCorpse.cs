﻿using UnityEngine;
using System.Collections;

public class EffectCorpse : MonoBehaviour {

	private Rigidbody2D _mover;
	private Vector2 _moveDirection;
	private float _speed;
	public float Friction;
	private float _startSpeed;
	// Use this for initialization
	void Start () {
		_mover = GetComponent<Rigidbody2D> ();
	}

	public void SetupCorpse(Vector2 startMove, float startSpeed)
	{
		_mover = GetComponent<Rigidbody2D> ();
		_moveDirection = startMove;
		_startSpeed = startSpeed;
		_speed = _startSpeed;
		_mover.AddTorque (Random.Range (-100, 100));
	}
	
	// Update is called once per frame
	void Update () {
		_mover.velocity = _moveDirection * _speed;	
		_speed = Mathf.Clamp (_speed - Friction, 0f, _startSpeed);
		if (Mathf.Abs (_speed)  < 1f && _startSpeed != 0f)
			Destroy (this);
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Terrain") 
		{
			var normal = coll.contacts[0].normal;
			
			if (normal == Vector2.up || normal == Vector2.down)
			{
				_moveDirection.y *= -1.2f;
			}
			
			if (normal == Vector2.left || normal == Vector2.right)
			{
				_moveDirection.x *= -1.2f;
			}
		}
	}
}
