﻿using UnityEngine;
using System.Collections;
using InControl;

public class PlayerMovement : MonoBehaviour
{
    public float MaxSpeed = 200f, 
        Acceleration = 30f,
        Friction = 30f;

    [HideInInspector]
    public Vector2 Direction;

    [HideInInspector]
    public bool CustomSpeed, CanSprint = true, FacingRight;

    [HideInInspector]
    public float Speed = 0f;


    private InputDevice _controller;
    private float _sprintTimer;
    private Vector2 _currentDirection;
    private Rigidbody2D _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        if (_controller == null)
            return;

        Direction = _controller.LeftStick.Vector;
        _currentDirection = Direction.magnitude > 0.2 ? Direction : _currentDirection;
        
        Speed = Direction.magnitude > 0.2 ? Mathf.Clamp(Speed + Acceleration, 0f, MaxSpeed) 
                : Speed = Mathf.Clamp(Speed - Friction, 0f, MaxSpeed);

        _rigidbody.velocity = _currentDirection.normalized * Speed;
    }

    public void SetController(InputDevice c)
    {
        _controller = c;
    }
}
