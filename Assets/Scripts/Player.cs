﻿using UnityEngine;
using System.Collections;
using InControl;

public class Player : MonoBehaviour
{
    public float Health;
    public int Ammo;
    public int Score;
    
	private InputDevice _controller;

    void Start()
    {
		_controller = InputManager.ActiveDevice;

        GetComponentInChildren<PlayerMovement>().SetController(_controller);
        GetComponentInChildren<CrossHairMovement>().SetController(_controller);

        if (_controller == null)
            gameObject.SetActive(false);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        switch (coll.gameObject.tag)
        {
            case "Enemy":
            	coll.gameObject.GetComponent<Alien>().TurnPlayer(gameObject, _controller);
                break;
        }
    }
}
