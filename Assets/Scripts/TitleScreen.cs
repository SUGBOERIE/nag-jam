﻿using UnityEngine;
using System.Collections;
using InControl;

public class TitleScreen : MonoBehaviour
{
	public int AssignedPlayers;
	
    void Update()
    {
		var device = InputManager.ActiveDevice;
        var start = device.GetControl(InputControlType.Start);

        if (start.WasPressed)
            Application.LoadLevel(1);
    }

    void Join()
    {
    	if (AssignedPlayers == 2)
    		return;

    	foreach (var d in InputManager.Devices)
    	{
    		if (d.Action1.WasPressed)
    		{
    			switch (AssignedPlayers)
    			{
    				case 0:
    					GameManager.Instance.Controllers.Add(d);
    					AssignedPlayers++;
    					break;

    				case 1:
    					if (GameManager.Instance.Controllers[AssignedPlayers - 1] != d)
    					{
    						GameManager.Instance.Controllers.Add(d);
    						AssignedPlayers++;
    						Application.LoadLevel(1);
    					}
    					break;
    			}
    		}
    	}
    }
}
