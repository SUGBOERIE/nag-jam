﻿using UnityEngine;
using System.Collections;

public class SpriteAssigner : MonoBehaviour {

	public Sprite[] Sprites;
	public LayerMask LayerMask;
	public LayerMask SecondMask;
	private SpriteRenderer ren;

	public struct State 
	{
		public bool left;
		public bool right;
		public bool top;
		public bool bottom;
		public bool center;
	}
	public State _state; 
	// Use this for initialization
	void Start () {
		ren = GetComponent<SpriteRenderer> ();

		AssignSprites ();
		Debug.Log (_state);
	}

	private void AssignSprites()
	{

		Vector2 origin = transform.position;
		float spriteSize = GetComponent<Renderer> ().bounds.size.x;
		RaycastHit2D leftRay = Physics2D.Raycast (origin, -transform.right, spriteSize + 1f, LayerMask);//Left
		RaycastHit2D rightRay = Physics2D.Raycast (origin, transform.right, spriteSize + 1f, LayerMask);//Right
		RaycastHit2D upRay = Physics2D.Raycast (origin, transform.up, spriteSize + 1f, LayerMask);//Up
		RaycastHit2D downRay = Physics2D.Raycast (origin, -transform.up, spriteSize + 1f, LayerMask);//Down


		bool leftSecond = true;
		bool rightSecond = true;
		bool topSecond = true;
		bool bottomSecond = true;
		if(SecondMask != null){
			leftSecond =Physics2D.Raycast (origin, -transform.right, spriteSize + 1f, LayerMask).collider != null;//Left
			rightSecond =Physics2D.Raycast (origin, transform.right, spriteSize + 1f, LayerMask).collider != null;//Right
			topSecond = Physics2D.Raycast (origin, transform.up, spriteSize + 1f, LayerMask).collider != null;//Up
			bottomSecond = Physics2D.Raycast (origin, -transform.up, spriteSize + 1f, LayerMask).collider != null;//Bottom
		}

		_state.left = leftRay.collider != null && leftSecond? true : false;
		_state.right = rightRay.collider != null  && rightSecond ? true : false;
		_state.top = upRay.collider != null  && topSecond? true : false;
		_state.bottom = downRay.collider != null && bottomSecond? true : false;
		_state.center = (_state.left && _state.right && _state.bottom && _state.top);
			
			if (_state.center) {
				ren.sprite = Sprites[0];
			}
			
			
			// -- one side empty -- //
			
			// top empty
			if (_state.right && !_state.top && _state.bottom && _state.left) {
				ren.sprite = Sprites[3];
			}
			
			// left emppty 
			if (_state.right && _state.top && _state.bottom && !_state.left) {
				ren.sprite = Sprites[14];
			}
			
			// right empty
			if (!_state.right && _state.top && _state.bottom && _state.left) {
				ren.sprite = Sprites[11];
			}
			
			// bottom empty
			if (_state.right && _state.top && !_state.bottom && _state.left) {
				ren.sprite = Sprites[12];
			}
			
			
			// -- two sides empty -- //
			
			// left and top empty
			if (_state.right && !_state.top && _state.bottom && !_state.left) {
				ren.sprite = Sprites[4];
			}
			
			// right and top empty
			if (!_state.right && !_state.top && _state.bottom && _state.left) {
				ren.sprite = Sprites[2];
			}
			
			// right and bottom empty
			if (!_state.right && _state.top && !_state.bottom && _state.left) {
				ren.sprite = Sprites[10];
			}
			
			// left and bottom empty
			if (_state.right && _state.top && !_state.bottom && !_state.left) {
				ren.sprite = Sprites[13];
			}
			
			// top and bottom empty 
			if (_state.right && !_state.top && !_state.bottom && _state.left) {
				ren.sprite = Sprites[7];
			}
			
			// left and right empty 
			if (!_state.right && _state.top && _state.bottom && !_state.left) {
				ren.sprite = Sprites[9];
			}
			
			
			// ---- three sides empty --- //
			
			// top and left and bottom empty
			if (_state.right && !_state.top && !_state.bottom && !_state.left) {
				ren.sprite = Sprites[6];
			}
			
			// top and right and bottom empty
			if (!_state.right && !_state.top && !_state.bottom && _state.left) {
				ren.sprite = Sprites[5];
			}
			
			// top and left and right empty
			if (!_state.right && !_state.top && _state.bottom && !_state.left) {
				ren.sprite = Sprites[1];
			}
			
			// bottom and left and right empty
			if (!_state.right && _state.top && !_state.bottom && !_state.left) {
				ren.sprite = Sprites[8];
			}
			
			
			
			
			// --- all sides missing --- //
			if (!_state.right && !_state.top && !_state.bottom && !_state.left) {
				ren.sprite = Sprites[0];
			}
	}
	
}
