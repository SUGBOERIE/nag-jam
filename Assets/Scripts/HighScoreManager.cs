﻿using UnityEngine;
using System.Collections;
using Gamelogic;

public class HighScoreManager : MonoBehaviour
{

    private StateMachine<HighScoreSceneState> _stateMachine;

    void Start()
    {
		_stateMachine = new StateMachine<HighScoreSceneState>();
		_stateMachine.AddState(HighScoreSceneState.SeeHighScore, SeeHighScoreStart, SeeHighScoreUpdate, SeeHighScoreStop);
		_stateMachine.AddState(HighScoreSceneState.EnterHighScore, null, null, null);

		if (GameManager.Instance.GameScore > GameManager.Instance.GetHighScore())
			_stateMachine.ChangeState(HighScoreSceneState.EnterHighScore);
		else
			_stateMachine.ChangeState(HighScoreSceneState.SeeHighScore);

		// set up high score UI
		// 
    }
	
    
    void Update()
    {
		_stateMachine.Update();
    }

    void SeeHighScoreStart()
    {

    }

    void SeeHighScoreUpdate()
    {
    	// wait for enter / press Action1 to go bact to title screen
    }

    void SeeHighScoreStop()
    {

    }

    void EnterHighScoreStart()
    {
    	// show enter high score panel
    }

    void EnterHighScoreUpdate()
    {
    	// wait for enter to be pressed
    }

    void EnterHighScoreStop()
    {
    	// move enter panel away 
    }
}
