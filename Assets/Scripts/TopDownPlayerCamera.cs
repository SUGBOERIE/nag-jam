﻿using UnityEngine;
using System.Collections;

public class TopDownPlayerCamera : MonoBehaviour
{
    public GameObject FollowObject;

    //Screenshake
    private bool _isShaking;
    private Vector3 _shakeVector;
    private float _curShakeTime;
    private float _shakeTime;

    // Use this for initialization
    void Start()
    {
	
    }
	
    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 shakeOffest = Vector3.zero;

        if (_isShaking)
        {
            shakeOffest = new Vector3(Random.Range(-_shakeVector.x, _shakeVector.x), Random.Range(-_shakeVector.y, _shakeVector.y), 0f);
            if (_curShakeTime <= 0)
            {
                _isShaking = false;
            }
            else
            {
                _curShakeTime -= Time.deltaTime;
            }
        }

        var followPos = FollowObject.transform.position + new Vector3(0f, 0f, -10f);
        transform.position = Vector3.Lerp(transform.position, followPos, 0.1f) + shakeOffest;
    }

    public void ShakeScreen(Vector3 shakeVector, float shakeTime, bool addToShake)
    {
        _isShaking = true;
        _shakeVector = addToShake ? _shakeVector + shakeVector : shakeVector;
        _curShakeTime = shakeTime;
    }
}
