﻿using UnityEngine;
using System.Collections;

public class HealthManager : MonoBehaviour
{

    public int Health;
    public bool DestroyDeath;

	public bool DestroyAfterFlash;
	public Material FlashMaterial;

	private Vector2 _deathDirection;
	
    public void DamageInstance(int damage, Vector3 damageDirection)
    {


        Health -= damage;
        if (Health <= 0) {
			if (DestroyDeath) {
				GameObject.Find ("GameManager").GetComponent<EffectManager> ().SpawnParticles (5, damageDirection, 5f, transform.position);
				_deathDirection = damageDirection;
				if (DestroyAfterFlash) {
					gameObject.AddComponent<EffectFlashColorPeriod> ();
					GetComponent<EffectFlashColorPeriod> ().SetupFlash (0.1f, true, FlashMaterial);
				} else {
					Destroy (gameObject);
				}
			}
		} else {
			gameObject.AddComponent<EffectFlashColorPeriod> ();
			GetComponent<EffectFlashColorPeriod> ().SetupFlash (0.1f, false, FlashMaterial);
		}
    }

	public Vector2 GetDeathDirection()
	{
		return Health <= 0? _deathDirection : Vector2.zero ;
	}
}
