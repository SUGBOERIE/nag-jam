﻿using UnityEngine;
using System.Collections;
using InControl;

public class CrossHairMovement : MonoBehaviour
{
	public InputDevice _controller;

	private Vector2 _currentDirection;

    void Update()
    {
    	if (_controller == null)
            return;
            
		var dir = _controller.RightStick.Vector;
    	_currentDirection = dir.magnitude > 0 ? dir : _currentDirection;
		var angle = Mathf.Atan2(_currentDirection.y, _currentDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void SetController(InputDevice c)
    {
    	_controller = c;
    }

	public Vector2 GetCurrentFacingDirection()
	{
		return _currentDirection;
	}
}
