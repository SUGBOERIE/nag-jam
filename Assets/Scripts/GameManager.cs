using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using InControl;
using Gamelogic;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public int TargetFrameRate = -1;
    public bool DEBUG;
    public BulletManager b;
    public int GameScore;

    public List<InputDevice> Controllers;
    public List<string> HighScores;

    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);

            Controllers = new List<InputDevice>();
            

            // var s = GLPlayerPrefs.GetIntArray("Base", "HighScoes");

            // if (s.Length == 0)
            // {
            //     GLPlayerPrefs.SetStringArray("Base", "HighScores", HighScores.ToArray());
            //     HighScores = new List<string>();
            // }
            // else
            // {
            //     HighScores = s.ToList();

            // }



            Application.targetFrameRate = TargetFrameRate;
            
        }
        else
        {
            if (this != _instance)
                Destroy(gameObject);
        }
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            Application.LoadLevel(Application.loadedLevel);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            switch(Application.loadedLevel)
            {
                case 0:
                    Application.Quit();
                    break;

                default:
                    Application.LoadLevel(0);
                    break;
            }
        }

        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
            DEBUG = !DEBUG;
        #endif
    }

    public int GetHighScore()
    {
        return 0;
    }
}
