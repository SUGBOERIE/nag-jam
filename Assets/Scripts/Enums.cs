
public enum WeaponType
{
    Pistol,
    Shotgun,
    Rifle,
    Automatic,
	Explosion,
	Laser,
	SoundWave
}

public enum AmmoType
{
	Normal,
	Exploding,
	Piercing,
	Confusion,
	Concussion,
	Laser, 
	Incendiary,
	SoundWave
}

public enum AlienHordeState
{
    AI,
    PLAYER
}

public enum LevelType
{
    MoonBase
}

public enum HighScoreSceneState
{
	SeeHighScore,
	EnterHighScore,
}